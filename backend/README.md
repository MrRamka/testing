# Testing-System - Backend (API)

----

### Development

---- 

Follow the following steps to prepare a development environment:

- Install Python 3.7.3: <https://www.python.org/downloads/>
- Install Postgresql <https://www.postgresql.org/download/>
- Install Microsoft C++ Build Tools <https://visualstudio.microsoft.com/visual-cpp-build-tools/>

Install all dependencies 

```bash
pip install -r requirements.txt
```

Copy all environments variables

```bash
copy example.env .env
```

Create database

Synchronize the database state with the current set of models and migrations. 

```bash
python manage.py migrate
```

Create superuser

```bash
python manage.py createsuperuser
```

Run server

```bash
python manage.py runserver
```

### Urls

---- 


:zap: Swagger <http://127.0.0.1:8000/>

:rocket: Admin <http://127.0.0.1:8000/admin/>

### Project structure

---- 

```
.
├── analysis                # Test and user statistics 
├── app                     # Main application, settings
├── core                    # Tests, questions and answers
├── statis                  # Staticfiles
├── user                    # User model and JWT authentication 
└── README.md
```
