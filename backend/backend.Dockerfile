FROM python:3.8

ENV PYTHONUNBUFFERED 1

ENV APP_ROOT /backend

WORKDIR ${APP_ROOT}

RUN apt-get update

RUN pip3 install -U pip

COPY requirements.txt ${APP_ROOT}/requirements.txt

RUN pip3 install -r ${APP_ROOT}/requirements.txt

# Set the working directory to /app
WORKDIR ${APP_ROOT}

# Copy the current directory contents into the container at /app
ADD . ${APP_ROOT}

RUN chmod 775 -R ${APP_ROOT}

CMD  ['python manage.py collectstatic --noinput', 'python manage.py runserver']