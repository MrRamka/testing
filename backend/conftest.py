import pytest
from rest_framework.test import APIClient
from core.models import Test, Question


@pytest.fixture()
def client():
    return APIClient()


@pytest.fixture(autouse=True)
def enable_db_for_all_tests(db):
    pass


@pytest.fixture
def fill_db():
    test = Test.objects.create(name='123', slug=None, author=None, id=1)
    quest = Question.objects.create(name='quest123', test=test, type=2, id=1)