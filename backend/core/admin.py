from django.contrib import admin

from core.models import Answer
from core.models import Question
from core.models import QuestionType
from core.models import Test
from core.models import TestSession
from core.models import UserAnswer


@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'created_at', 'updated_at', 'random_order', 'author']
    list_display_links = ['name']
    readonly_fields = ['slug']


@admin.register(QuestionType)
class QuestionTypeAdmin(admin.ModelAdmin):
    list_display = ['type']
    list_display_links = ['type']


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ['name', 'created_at', 'updated_at', 'test', 'type']
    list_display_links = ['name']


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ['value', 'is_correct', 'question', 'type']
    list_display_links = ['value']


@admin.register(UserAnswer)
class UserAnswerAdmin(admin.ModelAdmin):
    list_display = ['value', 'is_correct', 'question', 'type', "user", "session"]
    list_display_links = ['value', 'session', 'user']


@admin.register(TestSession)
class TestSessionAdmin(admin.ModelAdmin):
    list_display = ['user', 'test', 'session_hash', 'started_at', 'ended_at']
    list_display_links = ['user', 'test']
