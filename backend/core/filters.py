from django_filters import rest_framework as filters

from .models import Test
from django.db.models import Q


class TestFilter(filters.FilterSet):
    name_or_description = filters.CharFilter(method="substring_search")
    is_active = filters.BooleanFilter(field_name='isActive')
    ordering = filters.CharFilter(method="order_tests")

    class Meta:
        model = Test
        fields = ['name_or_description', 'is_active', 'ordering']

    def substring_search(self, queryset, name, value):
        return queryset.filter(
            Q(name__icontains=value) | Q(description__icontains=value)
        )

    def order_tests(self, queryset, name, value):
        if value == 'created':
            return queryset.order_by('created_at')
        elif value == 'updated':
            return queryset.order_by('updated_at')
        return queryset