import uuid
from django.db import models
from django.db.models import SET_NULL

from core.utils import slugify
from user.models import User
from django.contrib.postgres.fields import ArrayField
import uuid


class Test(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    slug = models.SlugField(null=True)
    random_order = models.BooleanField(null=True, default=False)
    author = models.ForeignKey(User, on_delete=SET_NULL, null=True)
    isActive = models.BooleanField(default=True)
    hash = models.UUIDField(unique=True, default=uuid.uuid4, editable=False, null=True)

    # if empty or len equals 0 they are sorted by creation date
    question_order = ArrayField(models.PositiveIntegerField(), blank=True, default=list)

    def _generate_unique_slug(self):
        unique_slug = slugify(self.name)
        num = 1
        while Test.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(unique_slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slug or len(self.slug) == 0:
            self.slug = self._generate_unique_slug()
        super().save(*args, **kwargs)

    def __str__(self) -> str:
        return f'{self.name} [{self.author}]'


SINGLE, MULTIPLE, TEXT, MATCHING, SEQUENCE = range(5)
TYPES = (
    (SINGLE, "Single"),
    (MULTIPLE, "Multiple"),
    (TEXT, "Text"),
    (MATCHING, "Matching"),
    (SEQUENCE, "Sequence"),
)


class QuestionType(models.Model):
    type = models.IntegerField(choices=TYPES, default=SINGLE)


class Question(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500, blank=True, null=True)
    image = models.ImageField(null=True, blank=True)
    explanation = models.CharField(max_length=500, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    test = models.ForeignKey(Test, on_delete=SET_NULL, null=True, related_name='questions')
    type = models.IntegerField(choices=TYPES, default=SINGLE)

    def __str__(self) -> str:
        return f'[Type: {TYPES[self.type][1]}] {self.name}'


class Answer(models.Model):
    value = models.CharField(max_length=250)
    is_correct = models.BooleanField(default=False)
    question = models.ForeignKey(Question, on_delete=SET_NULL, null=True, related_name='answers')
    type = models.IntegerField(choices=TYPES, default=SINGLE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_at']

    def __str__(self) -> str:
        return f'[{TYPES[self.type][1]}] [{self.question}] [{self.is_correct}] {self.value}'


class TestSession(models.Model):
    user = models.ForeignKey(User, on_delete=SET_NULL, null=True)
    started_at = models.DateTimeField(auto_now_add=True, verbose_name="Started at")
    ended_at = models.DateTimeField(null=True, blank=True, verbose_name="Ended at")
    test = models.ForeignKey(Test, on_delete=SET_NULL, null=True)
    session_hash = models.CharField(max_length=40)

    @staticmethod
    def _create_hash():
        return uuid.uuid4()

    def _generate_unique_hash(self):
        unique_hash = self._create_hash()
        while TestSession.objects.filter(session_hash=unique_hash).exists():
            unique_hash = self._create_hash()
        return unique_hash

    def save(self, *args, **kwargs):
        if not self.session_hash or len(self.session_hash) == 0:
            self.session_hash = self._generate_unique_hash()
        super().save(*args, **kwargs)

    def __str__(self):
        return f"Session-hash: {self.session_hash}"


class UserAnswer(models.Model):
    duration = models.TimeField(null=True)
    user = models.ForeignKey(User, on_delete=SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    session = models.ForeignKey(TestSession, on_delete=SET_NULL, null=True)
    value = models.CharField(max_length=250, null=True)
    is_correct = models.BooleanField(default=False)
    question = models.ForeignKey(Question, on_delete=SET_NULL, null=True, related_name='user_answers')
    type = models.IntegerField(choices=TYPES, default=SINGLE)

    def __str__(self) -> str:
        return f'[{self.user.__str__()}]'

    def save(self, *args, **kwargs):
        # from analysis.models import QuestionAnalysis
        # question_analysis = QuestionAnalysis.objects.get_or_create(question_id=self.answer.question_id)
        # question_analysis[0].save()
        super().save(*args, **kwargs)
