from rest_framework import permissions


SAFE_METHODS_ANSWER = ['GET', 'HEAD', 'OPTIONS', 'POST', 'PUT', 'PATCH']


class TestAnswerPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        if (request.method in SAFE_METHODS_ANSWER or
                request.user and
                request.user.is_authenticated):
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.user == request.user


class IsAuthenticatedOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        if (request.method in permissions.SAFE_METHODS or
                request.user and
                request.user.is_authenticated):
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:
            return True
        return obj.author == request.user
