from rest_framework import serializers

from .models import Answer
from .models import Question
from .models import Test
from .models import TestSession
from .models import UserAnswer


class TestSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Test
        fields = '__all__'


class AnswerSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Answer
        fields = '__all__'


class QuestionSerializer(serializers.ModelSerializer):
    answers = AnswerSerializer(many=True, read_only=True)

    class Meta(object):
        model = Question
        fields = ['id', 'name', 'description', 'image', 'explanation', 'created_at', 'updated_at', 'test', 'type',
                  'answers']


class PrivateAnswerSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Answer
        fields = ['value', 'question', 'type']


class UsersQuestionSerializer(serializers.ModelSerializer):
    answers = PrivateAnswerSerializer(many=True, read_only=True)

    class Meta(object):
        model = Question
        fields = ['id', 'name', 'description', 'image', 'explanation', 'created_at', 'updated_at', 'test', 'type',
                  'answers']


class UserQuestionSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = UserAnswer
        fields = "__all__"


class TestQuestionAnswerSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True, read_only=True)

    class Meta:
        model = Test
        fields = ['name', 'description', 'created_at', 'updated_at', 'slug', 'random_order', 'author', 'isActive',
                  'hash', 'question_order', 'questions']


class TestSessionSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = TestSession
        fields = "__all__"


class CreateTestSessionSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = TestSession
        fields = "__all__"
        extra_kwargs = {"session_hash": {"required": False, "allow_null": True}}
