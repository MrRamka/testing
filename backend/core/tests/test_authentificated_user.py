import pytest
from user.models import UserManager, User
from django.urls import reverse


@pytest.fixture()
def fill_db_user():
    user = User.objects.create_user('test', 'test@test.com', 'test')
    return user


@pytest.fixture
def client_with_auth(fill_db_user, client):
    user = fill_db_user
    client.force_authenticate(user=user)
    return client


def test_post_answer(client_with_auth, fill_db, fill_db_user):
    payload = {"value": "123", "duration": "00:00"}
    response = client_with_auth.post(reverse('user-question-answers-retrieve-update-destroy',
                                   kwargs={'test_id': 1, 'question_id': 1}), payload,
                           )
    data = response.data
    assert response.status_code == 201
    assert data['user'] == 1
    assert data['id'] == 1