import pytest
from rest_framework.generics import get_object_or_404

from core.models import Test, Question
from django.urls import reverse
from core.serializers import TestSerializer, UsersQuestionSerializer


def test_list_tests(client, fill_db):
    response = client.get(reverse('all-tests'))
    data = response.data

    tests = Test.objects.order_by('created_at').filter(author=None)
    serializer = TestSerializer(tests, many=True)

    assert response.status_code == 200
    assert data == serializer.data


def test_get_test(client, fill_db):
    response = client.get(reverse('test-retrieve-update-destroy', kwargs={'test_id': 1}))
    data = response.data

    obj = get_object_or_404(Test, pk=1)
    serializer = TestSerializer(obj)

    assert response.status_code == 200
    assert data == serializer.data


def test_get_questions(client, fill_db):
    response = client.get(reverse('user-question-list', kwargs={'test_id': 1}))
    data = response.data

    questions = Question.objects.order_by('created_at').filter(test_id=1)
    serializer = UsersQuestionSerializer(questions, many=True)

    assert response.status_code == 200
    assert data == serializer.data


def test_post_answer(client, fill_db):
    payload = {"value": "123", "duration": "00:00"}

    response = client.post(reverse('user-question-answers-retrieve-update-destroy',
                                   kwargs={'test_id': 1, 'question_id': 1}), payload)

    data = response.data

    assert response.status_code == 201
    assert data['user'] == None
    assert data['id'] == 2
