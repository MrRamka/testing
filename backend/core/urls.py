from django.urls import path

from core.views import QuestionAnswerCreateAPIView
from core.views import QuestionAnswerListAPIView
from core.views import QuestionAnswerRetrieveUpdateAPIView
from core.views import QuestionCreateAPIView
from core.views import QuestionListAPIView
from core.views import QuestionListForAnswerAPIView
from core.views import QuestionRetrieveUpdateAPIView
from core.views import QuestionUserAnswersListAPIView
from core.views import SingleTestRetrieveUpdateAPIView
from core.views import TestCreateAPIView
from core.views import TestListAPIView
from core.views import TestQuestionAnswerRetrieveAPIView
from core.views import TestSessionCreateAPIView
from core.views import TestSessionUpdateEndAPIView
from core.views import UserQuestionAnswerRetrieveUpdateAPIView


def trigger_error(request):
    raise IndexError


urlpatterns = [
    path('all-users-tests/', TestListAPIView.as_view(), name='all-tests'),
    path('test-new/', TestCreateAPIView.as_view(), name='test-create'),
    path('test/<uuid:hash>/', SingleTestRetrieveUpdateAPIView.as_view(), name='test-retrieve-update-destroy'),
    path('test-questions/<uuid:hash>/', QuestionListAPIView.as_view(), name='question-list'),
    path('test-users-questions/<uuid:hash>/', QuestionListForAnswerAPIView.as_view(), name='user-question-list'),
    path('questions/<uuid:hash>/<int:question_id>/', QuestionRetrieveUpdateAPIView.as_view(),
         name='question-retrieve-update-destroy'),
    path('test-question-answers/<uuid:hash>/<int:question_id>/', QuestionAnswerListAPIView.as_view(),
         name='question-answers-list'),
    path('question-answer/<uuid:hash>/new/',
         QuestionAnswerCreateAPIView.as_view(),
         name='question-answers-retrieve-update-destroy'),
    path('question-answer/<uuid:hash>/<int:answer_id>/',
         QuestionAnswerRetrieveUpdateAPIView.as_view(),
         name='question-answers-retrieve-update-destroy'),
    path('question-new/', QuestionCreateAPIView.as_view(), name='create-question'),
    path('user-question-answer/<uuid:hash>/<int:question_id>/', UserQuestionAnswerRetrieveUpdateAPIView.as_view(),
         name='user-question-answers-retrieve-update-destroy'),
    path('test-question-answer/<uuid:hash>/', TestQuestionAnswerRetrieveAPIView.as_view(),
         name='test-question-answers-retrieve'),
    path('sentry-debug/', trigger_error, name='trigger_error'),
    path('question-user-answers/<int:test_id>/<int:question_id>/', QuestionUserAnswersListAPIView.as_view(),
         name='question-user-answers-list'),
    path('session/start/', TestSessionCreateAPIView.as_view(), name='start-test-session'),
    path('session/', TestSessionUpdateEndAPIView.as_view(), name='update-end-test-session'),
]
