from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from django_filters import rest_framework as filters
from rest_framework.generics import CreateAPIView
from rest_framework.generics import ListAPIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.generics import RetrieveUpdateDestroyAPIView
from rest_framework.generics import UpdateAPIView
from rest_framework.generics import get_object_or_404

from core.filters import TestFilter
from core.models import Answer
from core.models import Question
from core.models import Test
from core.models import TestSession
from core.models import UserAnswer
from core.permissions import IsAuthenticatedOrReadOnly
from core.permissions import TestAnswerPermission
from core.serializers import AnswerSerializer
from core.serializers import CreateTestSessionSerializer
from core.serializers import QuestionSerializer
from core.serializers import TestQuestionAnswerSerializer
from core.serializers import TestSerializer
from core.serializers import TestSessionSerializer
from core.serializers import UserQuestionSerializer
from core.serializers import UsersQuestionSerializer


class TestListAPIView(ListAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]

    serializer_class = TestSerializer

    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TestFilter

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return Test.objects.order_by('created_at').filter(Q(author__email=self.request.user) | Q(author=None))
        else:
            return Test.objects.order_by('created_at').filter(author=None)


class TestCreateAPIView(CreateAPIView):
    serializer_class = TestSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class SingleTestRetrieveUpdateAPIView(RetrieveUpdateAPIView):
    """
        get: retrieve Test
        put: update Test
        patch: update Test
     """
    serializer_class = TestSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    http_method_names = ['get', 'put', 'patch']

    @transaction.atomic
    def get_object(self):
        hash = self.kwargs['hash']
        test_id = Test.objects.get(hash=hash).id
        obj = get_object_or_404(Test, pk=test_id)
        self.check_object_permissions(self.request, obj)
        return obj

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class QuestionListAPIView(ListAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]

    serializer_class = QuestionSerializer

    @transaction.atomic
    def get_queryset(self):
        hash = self.kwargs['hash']
        test_id = Test.objects.get(hash=hash).id
        test_owner_contains = Q(test__author=self.request.user)
        question_contains = Q(test_id=test_id)

        obj = get_object_or_404(Test, pk=test_id)
        self.check_object_permissions(self.request, obj)

        # TODO: ADD order
        return Question.objects.order_by('created_at').filter(question_contains & test_owner_contains)


class QuestionListForAnswerAPIView(ListAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]

    serializer_class = UsersQuestionSerializer

    @transaction.atomic
    def get_queryset(self):
        hash = self.kwargs['hash']
        test_id = Test.objects.get(hash=hash).id
        question_contains = Q(test_id=test_id)

        # непонятно зачем он нужен?
        obj = get_object_or_404(Test, pk=test_id)
        self.check_object_permissions(self.request, obj)

        # TODO: ADD order
        return Question.objects.order_by('created_at').filter(question_contains)


class QuestionCreateAPIView(CreateAPIView):
    serializer_class = QuestionSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class QuestionRetrieveUpdateAPIView(RetrieveUpdateDestroyAPIView):
    """
        get: retrieve Question
        put: update Question
        patch: update Question
     """
    serializer_class = QuestionSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    http_method_names = ['get', 'put', 'patch', 'delete']

    @transaction.atomic
    def get_object(self):
        hash = self.kwargs['hash']
        test_id = Test.objects.get(hash=hash).id
        question_id = self.kwargs['question_id']

        test_obj = get_object_or_404(Test, pk=test_id)
        self.check_object_permissions(self.request, test_obj)

        obj = get_object_or_404(Question, pk=question_id)
        return obj


class QuestionAnswerListAPIView(ListAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]

    serializer_class = AnswerSerializer

    @transaction.atomic
    def get_queryset(self):
        hash = self.kwargs['hash']
        test_id = Test.objects.get(hash=hash).id
        question_id = self.kwargs['question_id']

        obj = get_object_or_404(Test, pk=test_id)
        self.check_object_permissions(self.request, obj)
        return Answer.objects.filter(question_id=question_id).order_by("created_at")


class QuestionAnswerCreateAPIView(CreateAPIView):
    serializer_class = AnswerSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]


class QuestionAnswerRetrieveUpdateAPIView(RetrieveUpdateDestroyAPIView):
    """
        get: retrieve Question
        put: update Question
        patch: update Question
     """
    serializer_class = AnswerSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    http_method_names = ['get', 'put', 'patch', 'delete']

    @transaction.atomic
    def get_object(self):
        hash = self.kwargs['hash']
        test_id = Test.objects.get(hash=hash).id
        answer_id = self.kwargs['answer_id']

        test_obj = get_object_or_404(Test, pk=test_id)
        self.check_object_permissions(self.request, test_obj)

        obj = get_object_or_404(Answer, pk=answer_id)
        return obj


class UserQuestionAnswerRetrieveUpdateAPIView(RetrieveUpdateDestroyAPIView, CreateAPIView):
    """
        get: retrieve UserQuestion
        put: update UserQuestion
        patch: update UserQuestion
        post: create UserQuestion
     """
    serializer_class = UserQuestionSerializer
    permission_classes = [TestAnswerPermission]
    http_method_names = ['get', 'put', 'patch', 'delete', 'post']

    @transaction.atomic
    def get_object(self):
        hash = self.kwargs['hash']
        test_id = Test.objects.get(hash=hash).id
        question_id = self.kwargs['question_id']

        obj = get_object_or_404(Test, pk=test_id)
        self.check_object_permissions(self.request, obj)
        # для получения всех ответов использовать QuestionUserAnswersListAPIView
        return UserAnswer.objects.filter(answer__question_id=question_id)[0]

    def get_correct_answers(self):
        question_id = self.kwargs['question_id']
        return Answer.objects.filter(question_id=question_id)

    def check_user_answer(self, answer):
        all_answers = self.get_correct_answers()
        for q_answer in all_answers:
            # TODO Дополнительная проверка (убирать пробелы, MorphAnalyzer для строк)
            if answer == q_answer.value:
                return True
        return False

    @transaction.atomic
    def perform_create(self, serializer):
        data = self.request.data
        user_value = data["value"]
        session_hash = data['session_hash']
        question_id = self.kwargs['question_id']
        question = get_object_or_404(Question, id=question_id)
        session = get_object_or_404(TestSession, session_hash=session_hash)
        if session.ended_at is not None:
            # TODO Send error message. This session ended
            return
        is_correct = self.check_user_answer(user_value)
        if self.request.user.is_authenticated:
            serializer.save(user=self.request.user, session=session, value=user_value, question=question,
                            is_correct=is_correct)
        else:
            serializer.save(user=None, value=user_value, question=question, session=session, is_correct=is_correct)


class QuestionUserAnswersListAPIView(ListAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]

    serializer_class = UserQuestionSerializer

    @transaction.atomic
    def get_queryset(self):
        test_id = self.kwargs['test_id']
        question_id = self.kwargs['question_id']

        obj = get_object_or_404(Test, pk=test_id)
        self.check_object_permissions(self.request, obj)

        queryset = UserAnswer.objects.filter(answer__question_id=question_id).order_by('-created_at')
        sorted_items = []

        for ans in queryset:
            if not any(item.session == ans.session for item in sorted_items):
                sorted_items.append(ans)
        temp_ids = list(map(lambda ans: ans.id, sorted_items))
        queryset = UserAnswer.objects.filter(id__in=temp_ids).order_by('-created_at')

        return queryset


class TestSessionCreateAPIView(CreateAPIView):
    serializer_class = CreateTestSessionSerializer

    def perform_create(self, serializer):
        data = self.request.data
        test_hash = data["test_hash"]
        test = get_object_or_404(Test, hash=test_hash)
        author = self.request.user
        serializer.save(user=author, test=test)


class TestSessionUpdateEndAPIView(UpdateAPIView, RetrieveAPIView):
    serializer_class = TestSessionSerializer

    def perform_update(self, serializer):
        data = self.request.data
        session_hash = data['session_hash']
        test_session = get_object_or_404(TestSession, session_hash=session_hash)
        if test_session.ended_at is None:
            end_datetime = timezone.now()
            serializer.save(ended_at=end_datetime)

    def get_object(self):
        data = self.request.data
        session_hash = data['session_hash']
        return get_object_or_404(TestSession, session_hash=session_hash)


class TestQuestionAnswerRetrieveAPIView(RetrieveAPIView):
    """
        get: retrieve {Test: [Question: [Answer, Answer, Answer, ], Question: [Answer, Answer, Answer, ]]}
    """
    serializer_class = TestQuestionAnswerSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    http_method_names = ['get']

    @transaction.atomic
    def get_object(self):
        hash = self.kwargs['hash']
        test_id = Test.objects.get(hash=hash).id

        test_obj = get_object_or_404(Test, pk=test_id)

        return test_obj
