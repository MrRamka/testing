from user.unienv import get_token
from django.conf import settings
from django.shortcuts import redirect


def unienv_auth_callback(request):
    code = request.GET.get("code")
    token = get_token(request, code)
    return redirect(settings.FRONTEND_HOST_URL + settings.FRONTEND_PATH, token=token)

