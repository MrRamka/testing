from drf_yasg import openapi
from rest_framework import serializers

from .models import User
from django.contrib.auth import authenticate


class RegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=128,
        min_length=8,
        write_only=True
    )

    token = serializers.CharField(max_length=255, read_only=True)

    class Meta:
        model = User
        fields = ['email', 'username', 'password', 'token']

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class UserField(serializers.JSONField):
    class Meta:
        write_only = True
        swagger_schema_fields = {
            "type": openapi.TYPE_OBJECT,
            "title": "User data",
            "properties": {
                "email": openapi.Schema(
                    title="email",
                    type=openapi.TYPE_STRING,
                ),
                "password": openapi.Schema(
                    title="password",
                    type=openapi.TYPE_STRING,
                ),
            },
        }


class LoginSerializer(serializers.Serializer):
    user = UserField(required=False)
    email = serializers.CharField(max_length=255, read_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    class Meta:
        fields = ["user", "email", "token"]


    def validate(self, data):
        user = data.get("user", None)
        if user is None:
            raise serializers.ValidationError(
                "Некорректные данные."
            )
        serializer = LoginInnerSerializer(data=user)
        serializer.is_valid(raise_exception=True)
        return serializer.data


class LoginInnerSerializer(serializers.ModelSerializer):
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)
    token = serializers.CharField(max_length=255, read_only=True)

    class Meta:
        model = User
        fields = ['email', 'password', 'token']

    def validate(self, data):
        email = data.get('email', None)
        password = data.get('password', None)

        if email is None:
            raise serializers.ValidationError(
                'Необходимо ввести ваш email.'
            )

        if password is None:
            raise serializers.ValidationError(
                'Необходимо ввести пароль.'
            )

        user = authenticate(username=email, password=password)

        if user is None:
            raise serializers.ValidationError(
                'Пользователь с такими данными не найден.'
            )

        if not user.is_active:
            raise serializers.ValidationError(
                'Пользователь не активен.'
            )
        return {
            'email': user.email,
            'token': user.token
        }
