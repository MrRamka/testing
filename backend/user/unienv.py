import requests
from django.conf import settings
from django.http import HttpRequest
from django.urls import reverse

UNIENV_HOST = "https://uenv-core.kpfu.ru"


def _get_redirect_uri(request):
    return request.build_absolute_uri(reverse("unienv-auth-callback"))


def get_authorization_start_url(request: HttpRequest) -> str:
    callback_url = request.build_absolute_uri(reverse("unienv-auth-callback"))
    return (
        f"{UNIENV_HOST}/oauth/authorize?"
        f"{UNIENV_HOST}/oauth/authorize/?"
        f"response_type=code&"
        f"client_id={settings.UNIENV_CLIENT_ID}&"
        f"redirect_uri={callback_url}"
        f"redirect_uri={_get_redirect_uri(request)}"
    )


def get_token(request: HttpRequest, code: str):
    response = requests.post(
        f"{UNIENV_HOST}/oauth/token/",
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data={
            "client_id": settings.UNIENV_CLIENT_ID,
            "client_secret": settings.UNIENV_CLIENT_SECRET,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": _get_redirect_uri(request),
        },
    )
    response.raise_for_status()
    response_data = response.json()
    return response_data["access_token"]
