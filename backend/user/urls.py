from django.urls import path

from user.views import CreateUserView, LoginAPIView
from user.auth import unienv_auth_callback

urlpatterns = [
    path('registration/', CreateUserView.as_view(), name='registration'),
    path("login/unienv_auth_callback/", unienv_auth_callback, name="unienv-auth-callback"),
    path('login/', LoginAPIView.as_view(), name='login'),
]
