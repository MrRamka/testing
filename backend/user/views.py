from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.generics import CreateAPIView, GenericAPIView
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from user.models import User
from user.renderers import UserJSONRenderer
from user.serializers import RegistrationSerializer, LoginSerializer


class CreateUserView(CreateAPIView):
    model = User
    permission_classes = (AllowAny,)
    authentication_classes = ()
    serializer_class = RegistrationSerializer


class LoginAPIView(GenericAPIView):
    permission_classes = (AllowAny,)
    renderer_classes = (UserJSONRenderer,)
    serializer_class = LoginSerializer

    response_schema_dict = {
        "200": openapi.Response(
            description="custom 200 description",
            examples={
                "application/json": {
                    "email": "some_email@mail.ru",
                    "token": "mega_long_token",
                }
            }
        )
    }

    @swagger_auto_schema(responses=response_schema_dict)
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(serializer.data, status=status.HTTP_200_OK)
