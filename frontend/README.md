# Testing System - Frontend

----

### Development

Follow the following steps to prepare a development environment:

- Install Node 14.x: <https://nodejs.org/en/download/> or <https://github.com/nvm-sh/nvm>
- Install Yarn <https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable>


Install all dependencies

```bash
yarn
```

Copy all environments variables

```bash
copy example.env .env
```

Run project for development

```bash
yarn dev
```


Open :rocket: [http://localhost:3000](http://localhost:3000) to view it in the browser.


```bash
yarn build
```

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

```bash
yarn serve
```

Starts built app with `serve` static server.

### Ветки

- `master` - ветка разработки, автоматически деплоится на стейджинг
- `prod` - ветка с версией для промышленного использования, автоматически деплоится на прод
