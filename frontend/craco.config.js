const CracoLessPlugin = require('craco-less');

module.exports = {
	style: {
		postcss: {
			plugins: [
				require('tailwindcss'),
				require('autoprefixer'),
				require('@tailwindcss/forms'),
			],
		},
	},
	plugins: [
		{
			plugin: CracoLessPlugin,
			options: {
				lessLoaderOptions: {
					lessOptions: {
						modifyVars: { '@primary-color': '#0050B3' },
						javascriptEnabled: true,
					},
				},
			},
		},
	],
}
