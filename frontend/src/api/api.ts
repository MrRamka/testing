import { AxiosResponse } from "axios";
import { ResponseResult } from "./types";
import { Routes } from "../constants";

function helpResponseDataTransformer<TData>(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  responseData: any,
  status?: number
): ResponseResult<TData> {
  let data: ResponseResult<TData>;
  if (responseData.success) {
    data = {
      data: responseData.data,
      message: responseData.message,
      success: true,
      code: status ?? 200,
    };
  } else {
    data = {
      data: responseData.data,
      message: responseData.message,
      success: false,
      code: status,
    };
  }
  return data;
}

export async function makeRequest<TData>(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  requester: () => Promise<AxiosResponse<any>>
): Promise<ResponseResult<TData>> {
  try {
    const result = await requester();
    return helpResponseDataTransformer<TData>(result.data, result.status);
  } catch (e) {
    if (e.response?.status === 401 || e.response?.status === 403) {
      window.location.replace(Routes.LOGIN);
    }
    return { success: false, data: null };
  }
}

export const isSuccessfulAndWithData = (
  response: ResponseResult<any>
): boolean => {
  return response && response.data != null && response.success;
};
