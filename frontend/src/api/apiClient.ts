import axios from "axios";

const apiClient = axios.create({
  baseURL: `${process.env.REACT_APP_API_URL}/v1.0/`,
});

apiClient.interceptors.request.use((config) => {
  if (config.url) {
    const token = localStorage.getItem("access-token");
    config.headers.Authorization = `Token ${token}`;
  }

  return config;
});

export { apiClient };
