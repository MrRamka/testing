import { AxiosRequestConfig } from "axios";

export type SuccessResponseResult<TData> = {
  code?: number;
  success: true;
  message?: string;
  data: TData | null;
};

export type ErrorResponseResult<TData> = {
  code?: number;
  success: false;
  message?: string;
  data: TData | null;
};

export type ResponseResult<TData> =
  | SuccessResponseResult<TData>
  | ErrorResponseResult<TData>;

export interface ExtendedAxiosResponse<T = any> {
  data: T;
  status: number;
  statusText: string;
  headers: any;
  config: AxiosRequestConfig;
  success?: boolean;
  request?: any;
}
