export interface AnswerResponse {
  id: number;
  value: string;
  is_correct: boolean;
  type: number;
  question: number;
}

export interface AnswerResponseStat extends AnswerResponse {
  answers_count: number;
}
