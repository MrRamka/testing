import React, {useCallback, useEffect, useMemo, useState} from "react";
import { Form, Input, Checkbox, Button, message, List, Card } from "antd";
import axios from "axios";
import { Routes, Colors } from "../../constants";
import { CardTestItem, StyledContent } from "../../pages";

interface SettingsProps {
  data?: CardTestItem;
  isLoading: boolean;
}

interface Interpretation {
  slug: string;
  name: string;
  description: string;
}

export const Settings = ({ isLoading, data }: SettingsProps): JSX.Element => {
  const config = useMemo(() => ({
    random_order: data?.random_order,
    isActive: data?.isActive,
    public: data?.public,
  }), [data]);

  const testUrl = String(
    window.location.host + Routes.START_TEST_PREFIX + data?.hash
  );
  const onFinishCopyLink = () => {
    window.navigator.clipboard
      .writeText(testUrl)
      .then(() => {
        message.success("Ссылка скопирована!");
      })
      .catch(() => {
        message.error("Не удалось скопировать ссылку!");
      });
  };

  const onFinishFailedCopyLink = () => {
    message.error("Не удалось скопировать ссылку!");
  };

  const user_token = localStorage.getItem("access-token");

  const [questionInterpretationList, setQuestionInterpretationList] = useState<
    Array<Interpretation>
  >([]);
  const [questionInterpretation, setQuestionInterpretation] = useState<
    string | null
  >(null);

  useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_API_URL}/v1.0/core/test-interpretations/`)
      .then((data) => {
        setQuestionInterpretationList(data.data.data);
      })
      .catch((error) => {
        console.log(error.response.data);
      });
  }, []);

  const onSubmit = useCallback(
    (values) => {
      axios
        // .get(
        //     `${process.env.REACT_APP_API_URL}/v1.0/core/test-settings/` + data?.hash + '/',
        //   {
        //       headers: {Authorization: "Token " + user_token}})
        .put(
          `${process.env.REACT_APP_API_URL}/v1.0/core/test-settings/` +
            data?.hash +
            "/",
          {
            random_order: config.random_order,
            isActive: config.isActive,
            public: config.public,
            interpretation_type: questionInterpretation,
          },
          { headers: { Authorization: "Token " + user_token } }
        )
        .then((data) => {
          if (data.data.success) {
            message.success("настройки сохранены");
          }
        });
      // .then(() => {
      // })
      // .catch((error) => {
      // });
    },
    [questionInterpretation, config, user_token, data]
  );

  const onChangePublic = (e: any) => {
    config.public = !config.public;
  };
  const onChangeActive = (e: any) => {
    config.isActive = !config.isActive;
  };
  const onChangeRandom = (e: any) => {
    config.random_order = !config.random_order;
  };

  const public_label = "Публичный";
  const active_label = "Неактивный";
  const random_label = "Случайный порядок вопросов";

  return (
    <StyledContent>
      <div
        className='flex justify-center'
        style={{ padding: 24, paddingTop: 0 }}
      >
        <div className={"w-1/2"}>
          <Card
            title={"Ссылка на тест"}
            className={"rounded-lg cursor-pointer shadow-md hover:shadow-xl"}
            style={{
              width: "100%",
              borderColor: Colors.blue_8,
              marginTop: "24px",
            }}
          >
            <Form
              onFinish={onFinishCopyLink}
              onFinishFailed={onFinishFailedCopyLink}
              autoComplete='off'
            >
              <Form.Item name='url' label='URL'>
                <Input type='text' defaultValue={testUrl} readOnly={true} />
              </Form.Item>
              <Form.Item>
                <Button type='primary' htmlType='submit'>
                  Скопировать ссылку
                </Button>
              </Form.Item>
            </Form>
          </Card>
          <Form onFinish={onSubmit}>
            <Card
              title={"Настройки теста"}
              className={"rounded-lg cursor-pointer shadow-md hover:shadow-xl"}
              style={{
                width: "100%",
                borderColor: Colors.blue_8,
                marginTop: "24px",
              }}
            >
              <Form.Item name='public'>
                <Checkbox onChange={onChangePublic}> {public_label} </Checkbox>
              </Form.Item>
              <Form.Item name='active'>
                <Checkbox onChange={onChangeActive}>{active_label}</Checkbox>
              </Form.Item>
              <Form.Item name='random'>
                <Checkbox onChange={onChangeRandom}>{random_label}</Checkbox>
              </Form.Item>
            </Card>
            <Form.Item>
              <List itemLayout='horizontal'>
                <List.Item>
                  <Card
                    title={"Без интерпретации"}
                    className={
                      questionInterpretation === null
                        ? "rounded-lg cursor-pointer shadow-md hover:shadow-xl border-b-8"
                        : "rounded-lg cursor-pointer shadow-md hover:shadow-xl border-0"
                    }
                    style={{ width: "100%", borderColor: Colors.blue_8 }}
                    onClick={() => setQuestionInterpretation(null)}
                  >
                    Интерпретация не требуется
                  </Card>
                </List.Item>
                {questionInterpretationList.map((interpretation, idx) => (
                  <List.Item key={idx}>
                    <Card
                      title={interpretation.name}
                      className={
                        questionInterpretation === interpretation.slug
                          ? "rounded-lg cursor-pointer shadow-md hover:shadow-xl border-b-8"
                          : "rounded-lg cursor-pointer shadow-md hover:shadow-xl border-0"
                      }
                      style={{ width: "100%", borderColor: Colors.blue_8 }}
                      onClick={() =>
                        setQuestionInterpretation(interpretation.slug)
                      }
                    >
                      {interpretation.description}
                    </Card>
                  </List.Item>
                ))}
              </List>
            </Form.Item>
            <Form.Item>
              <Button type='primary' htmlType='submit'>
                Сохранить
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </StyledContent>
  );
};
