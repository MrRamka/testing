import React from "react";
import logo from "../../images/kfu_logo_white_full.svg";
import longLogo from "../../images/kfu_logo_l_rus.svg";
import { StyledAntHeader } from "./styles";
import { Routes as R } from "../../constants";
import { useLocation } from "react-router-dom";

export const PageHeader = (): JSX.Element => {
  const location = useLocation();

  const isLoginOrRegistration =
    location.pathname.startsWith(R.LOGIN) ||
    location.pathname.startsWith(R.REGISTRATION);

  return (
    <>
      {isLoginOrRegistration ? (
        <StyledAntHeader>
          <img
            className='my-1'
            src={logo}
            alt='Казанский федеральный университет'
          />
        </StyledAntHeader>
      ) : (
        <StyledAntHeader>
          <div className='my-2 flex h-full'>
            <img
              className='h-3/4'
              src={longLogo}
              alt='Казанский федеральный университет'
            />
          </div>
        </StyledAntHeader>
      )}
    </>
  );
};
