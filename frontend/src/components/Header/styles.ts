import { Layout } from "antd";
import styled from "styled-components";
import { Colors } from "../../constants";

const { Header } = Layout;

export const StyledAntHeader = styled(Header)`
  background-color: ${Colors.blue_8};
`;
