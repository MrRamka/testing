import styled from "styled-components";
import { Layout } from "antd";
import { Colors } from "../../constants";

export const PageLayout = styled(Layout)`
  background-color: ${Colors.gray_1};
`;
