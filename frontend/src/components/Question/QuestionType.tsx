import React, { useCallback } from "react";
import { IQuestionTypes } from "./types";
import { Select } from "antd";

interface QuestionTypeProps {
  type: IQuestionTypes;
  isActive: boolean;
  updateQType: (type: IQuestionTypes) => void;
}

const QuestionTypeColors = {
  [IQuestionTypes.TEXT]: "bg-green-100",
  [IQuestionTypes.MATCHING]: "bg-red-100",
  [IQuestionTypes.MULTIPLE]: "bg-yellow-100",
  [IQuestionTypes.SINGLE]: "bg-blue-100",
  [IQuestionTypes.SEQUENCE]: "bg-pink-100",
  [IQuestionTypes.PARAGRAPH]: "bg-orange-100",
};

const getColorByType = (type: IQuestionTypes): string => {
  switch (type) {
    case IQuestionTypes.MATCHING:
      return "bg-red-100";
    case IQuestionTypes.MULTIPLE:
      return "bg-yellow-100";
    case IQuestionTypes.SEQUENCE:
      return "bg-pink-100";
    case IQuestionTypes.TEXT:
      return "bg-green-100";
    case IQuestionTypes.SINGLE:
      return "bg-blue-100";
    case IQuestionTypes.PARAGRAPH:
      return "bg-orange-100";
    default:
      return "bg-gray-100";
  }
};

export const QuestionType = ({
  type,
  isActive,
  updateQType,
}: QuestionTypeProps): JSX.Element => {
  const onSelect = useCallback(
    (_, value) => {
      if (value) {
        updateQType(value.key as IQuestionTypes);
      }
    },
    [updateQType]
  );

  return (
    <>
      <Select
        value={type}
        className={`font-semibold rounded-md w-full ${getColorByType(type)}`}
        disabled={!isActive}
        bordered={false}
        onSelect={onSelect}
      >
        {Object.keys(QuestionTypeColors).map((item) => (
          <Select.Option key={item} value={item} className={`font-semibold`}>
            {item}
          </Select.Option>
        ))}
      </Select>
    </>
  );
};
