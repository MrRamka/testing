import { AnswerResponse } from "../Answer/types";

export enum IQuestionTypes {
  SINGLE = "Single",
  MULTIPLE = "Multiple",
  TEXT = "Text",
  MATCHING = "Matching",
  SEQUENCE = "Sequence",
  PARAGRAPH = "Paragraph",
  SYSTEM = "System",
}

export interface IQuestion {
  id: number;
  type: number;
  name: string;
  description?: string;
  image?: string;
  explanation?: string;
  created_at: string;
  updated_at: string;
  answers?: AnswerResponse[];
  question_order?: number[];
  is_required?: boolean;
}

export function getQuestionTypeByNumberType(type: number): IQuestionTypes {
  switch (type) {
    case 0:
      return IQuestionTypes.SINGLE;
    case 1:
      return IQuestionTypes.MULTIPLE;
    case 2:
      return IQuestionTypes.TEXT;
    case 3:
      return IQuestionTypes.MATCHING;
    case 4:
      return IQuestionTypes.SEQUENCE;
    case 5:
      return IQuestionTypes.PARAGRAPH;
    case 6:
      return IQuestionTypes.SYSTEM;
  }
  return IQuestionTypes.SINGLE;
}

export function getNumberTypeByQuestionType(type: IQuestionTypes): number {
  switch (type) {
    case IQuestionTypes.SINGLE:
      return 0;
    case IQuestionTypes.MULTIPLE:
      return 1;
    case IQuestionTypes.TEXT:
      return 2;
    case IQuestionTypes.MATCHING:
      return 3;
    case IQuestionTypes.SEQUENCE:
      return 4;
    case IQuestionTypes.PARAGRAPH:
      return 5;
    case IQuestionTypes.SYSTEM:
      return 6;
  }
  return 0;
}
