import { AnswerResponse } from "../../Answer/types";
import { MutableRefObject } from "react";

export interface SingleAnswerProps {
  answer: AnswerResponse;
  isActive: boolean;
  inputRef: MutableRefObject<any> | null;
  isChangeCorrectAnswers: boolean;
  updateAnswerValue: (value: AnswerResponse) => void;
}
