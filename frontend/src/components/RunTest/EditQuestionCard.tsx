import React, { useCallback, useEffect, useRef } from "react";
import { CardTestItem } from "../../pages";
import {
  getQuestionTypeByNumberType,
  IQuestion,
  IQuestionTypes,
} from "../Question/types";

import { Checkbox, Form, Input, Radio, Space } from "antd";
import { useMutation } from "react-query";
import { UserAnswer } from "./types";
import { useRecoilValue } from "recoil";
import { currentTestHash } from "../../recoil/selectors";
import { createUserAnswerRequest } from "../../api/test";
import { debounce } from "lodash";
import { Colors } from "../../constants";
import { makeRequiredFormFieldRule } from "../../constants/rules";

const { TextArea } = Input;

interface EditQuestionCardProps {
  testData: CardTestItem;
  question?: IQuestion;
  isActive: boolean;
  sessionHash: string;
}

export const EditQuestionCard = ({
  question,
  sessionHash,
}: EditQuestionCardProps): JSX.Element => {
  const testHash = useRecoilValue(currentTestHash);

  const saveAnswerMutation = useMutation(async (userAnswerData: UserAnswer) => {
    return createUserAnswerRequest(
      userAnswerData,
      testHash,
      question?.id ?? 0
    ).catch((e) => console.log(e));
  });

  const debouncedSearch = useRef(
    debounce(async (value) => {
      if (!question) {
        return;
      }
      const answer: Record<string, any> = {
        [question.type]: value,
      };
      const newData: UserAnswer = {
        session_hash: sessionHash,
        type: question.type,
        answer,
      };
      saveAnswerMutation.mutate(newData);
    }, 500)
  ).current;

  const onTextValueChange = useCallback(
    (e) => {
      debouncedSearch(e.target.value);
    },
    [debouncedSearch]
  );

  useEffect(() => {
    return () => {
      debouncedSearch.cancel();
    };
  }, [debouncedSearch]);

  const onSingleAnswerChanged = useCallback(
    (event) => {
      if (!question) {
        return;
      }
      const answer: Record<string, any> = {
        [question.type]: event.target.value,
      };
      const newData: UserAnswer = {
        session_hash: sessionHash,
        type: question.type,
        answer: answer,
      };
      saveAnswerMutation.mutate(newData);
    },
    [saveAnswerMutation, question, sessionHash]
  );

  const onMultipleAnswerChanged = useCallback(
    (values) => {
      if (!question) {
        return;
      }
      const answer: Record<string, any> = {
        [question.type]: values,
      };
      const newData: UserAnswer = {
        session_hash: sessionHash,
        type: question.type,
        answer: answer,
      };
      saveAnswerMutation.mutate(newData);
    },
    [saveAnswerMutation, question, sessionHash]
  );

  if (!question) {
    return <></>;
  }

  const drawConstructorAnswers = () => {
    switch (getQuestionTypeByNumberType(question.type)) {
      case IQuestionTypes.TEXT:
        return (
          <>
            <p>Краткий ответ</p>
            <p>Ваш ответ: </p>
            <Input size={"small"} onChange={onTextValueChange} />
          </>
        );
      case IQuestionTypes.SINGLE:
        return (
          <>
            <Radio.Group onChange={onSingleAnswerChanged}>
              <Space direction='vertical'>
                {question?.answers?.map((item) => {
                  return (
                    <Radio
                      key={item.value + "_" + item.id + "_" + Math.random()}
                      value={item.id}
                    >
                      {item.value}
                    </Radio>
                  );
                })}
              </Space>
            </Radio.Group>
          </>
        );

      case IQuestionTypes.MULTIPLE:
        return (
          <>
            <Checkbox.Group onChange={onMultipleAnswerChanged}>
              <Space direction='vertical'>
                {question?.answers?.map((item) => {
                  return (
                    <div key={`answer_radio_${item.id}_${item.value}`}>
                      <Checkbox
                        key={item.value + "_" + item.id}
                        value={item.id}
                      >
                        {item.value}
                      </Checkbox>
                    </div>
                  );
                })}
              </Space>
            </Checkbox.Group>
          </>
        );
      case IQuestionTypes.PARAGRAPH:
        return (
          <TextArea
            placeholder='Введите ответ'
            autoSize={{ minRows: 2, maxRows: 6 }}
            onChange={onTextValueChange}
          />
        );
      default:
        return <p>Not implemented</p>;
    }
  };

  return (
    <Form.Item
      id={question?.name}
      name={question?.name}
      rules={
        question?.is_required
          ? [makeRequiredFormFieldRule("Это обязательный вопрос!")]
          : []
      }
    >
      {question.is_required ? (
        <div className='font-semibold'>
          <div
            style={{
              color: Colors.red_8,
              display: "inline-block",
              fontSize: 18,
            }}
          >
            *
          </div>
          {question.name}
        </div>
      ) : (
        <div className='font-semibold'>{question.name}</div>
      )}
      {drawConstructorAnswers()}
    </Form.Item>
  );
};
