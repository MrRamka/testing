import React, { useCallback, useState, useEffect } from "react";
import { CardTestItem } from "../../pages";
import { useMutation, useQuery } from "react-query";
import {getNumberTypeByQuestionType, IQuestion, IQuestionTypes} from "../Question/types";
import { CardType } from "../QuestionCard";
import { RunQuestionCard } from "./RunQuestionCard";
import { Colors, getCoreUserQuestionsAPI } from "../../constants";
import { Button, Form, message, Modal } from "antd";
import { noop } from "../../utils";
import { useHistory, useLocation } from "react-router-dom";
import { useRecoilValue } from "recoil";
import { currentTestHash } from "../../recoil/selectors";
import { endSessionRequest } from "../../api/test";

interface TestQuestionsProps {
  data: CardTestItem;
  sessionHash: string;
}

export const TestQuestions = ({
  data,
  sessionHash,
}: TestQuestionsProps): JSX.Element => {
  const [currentSelectedCard, setCurrentSelectedCard] = useState<number>(-1);
  const history = useHistory();
  const location = useLocation();
  const testHash = useRecoilValue(currentTestHash);

  const queryParams = new URLSearchParams(location.search);
  const params = Array.from(queryParams)
    .filter((x) => x[0].startsWith("question_"))
    .map((x) => ({ name: x[0].replace("question_", ""), value: x[1] }));

  const [form] = Form.useForm();
  useEffect(() => {
    if (params.length > 0) {
      form.setFields(params);
    }
  }, [form, params]);

  const { data: questionData } = useQuery<IQuestion[]>(
    getCoreUserQuestionsAPI(testHash),
    {
      enabled: data?.id != null && data?.id !== 0,
    }
  );

  const endSessionMutation = useMutation(async () => {
    return endSessionRequest(sessionHash);
  });

  const leaveTest = useCallback(() => {
    history.push("/");
  }, [history]);

  const onEndTest = useCallback(async () => {
    await endSessionMutation.mutate();
    Modal.info({
      title: "Вы прошли тест",
      okText: "Перейти на главную страницу",
      width: 500,
      onOk() {
        leaveTest();
      },
    });
    message.success("Ответы сохранены и отправлены");
  }, [endSessionMutation, leaveTest]);

  const questions = (questionData ?? [])
      .filter(question => question.type !== getNumberTypeByQuestionType(IQuestionTypes.SYSTEM));

  return (
    <div className='w-1/2'>
      <RunQuestionCard
        type={CardType.TITLE}
        testData={data}
        onCardClick={noop}
        isActive={false}
        sessionHash={sessionHash}
      />
      <Form
        form={form}
        name={"question_form"}
        layout={"vertical"}
        initialValues={{ remember: true }}
        autoComplete='off'
        onFinish={onEndTest}
        requiredMark={true}
      >
        <div>
          {questions.map((question, idx) => (
            <RunQuestionCard
              type={CardType.QUESTION}
              key={question.id}
              isActive={currentSelectedCard === idx}
              question={question}
              onCardClick={() => setCurrentSelectedCard(idx)}
              testData={data}
              sessionHash={sessionHash}
            />
          ))}
        </div>
        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            className='login-form-button'
            style={{ backgroundColor: Colors.blue_8 }}
          >
            Отправить
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
