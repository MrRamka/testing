type Answer = Record<string, any>;

export interface UserAnswer {
  session_hash: string;
  type: number;
  answer: Answer;
}

export interface Session {
  session_hash: string;
  started_at: string;
  test: number;
  user: number;
  ended_at: string;
}
