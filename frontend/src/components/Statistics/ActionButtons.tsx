import styled from "styled-components";
import { Button } from "antd";
import { Colors } from "../../constants";
import React from "react";

interface BaseButtonProps {
  $primaryColor: string;
  $secondaryColor: string;
  $borderColor: string;
}

const BaseButton = styled(Button)<BaseButtonProps>`
  color: ${(props) => props.$primaryColor};
  box-shadow: unset;
  border: 1px solid ${(props) => props.$borderColor};

  &:hover,
  &:active,
  &:focus {
    background-color: ${(props) => props.$secondaryColor};
    color: ${(props) => props.$primaryColor};
    border: 1px solid ${(props) => props.$borderColor};
  }
`;

export enum ACTION_BUTTONS_TYPE {
  PRIMARY = "PRIMARY",
  SUCCESS = "SUCCESS",
  DANGER = "DANGER",
  WARNING = "WARNING",
}

interface ActionButtonsProps {
  type: ACTION_BUTTONS_TYPE;
  children: React.ReactNode;
}

const colors = {
  [ACTION_BUTTONS_TYPE.PRIMARY]: {
    $primaryColor: Colors.blue_8,
    $secondaryColor: Colors.blue_1,
    $borderColor: Colors.blue_5,
  },
  [ACTION_BUTTONS_TYPE.SUCCESS]: {
    $primaryColor: Colors.green_8,
    $secondaryColor: Colors.green_1,
    $borderColor: Colors.green_5,
  },
  [ACTION_BUTTONS_TYPE.WARNING]: {
    $primaryColor: Colors.orange_8,
    $secondaryColor: Colors.orange_1,
    $borderColor: Colors.orange_5,
  },
  [ACTION_BUTTONS_TYPE.DANGER]: {
    $primaryColor: Colors.red_8,
    $secondaryColor: Colors.red_1,
    $borderColor: Colors.red_5,
  },
};

export const ActionButtons = ({
  type,
  children,
}: ActionButtonsProps): JSX.Element => {
  return (
    <BaseButton {...colors[type]} className='border-0 rounded-lg mr-2'>
      {children}
    </BaseButton>
  );
};
