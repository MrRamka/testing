import { Tag } from "antd";
import React from "react";
import { Tooltip } from "../Tooltip";

export enum PROBLEMS {
  GUESS,
  DIFF_IDX,
  CORRECT_ANSWERS,
}

const Problems = {
  [PROBLEMS.GUESS]: {
    short: "Угадывание",
    long: "Критерий угадывания",
    color: "magenta",
  },
  [PROBLEMS.DIFF_IDX]: {
    short: "ИД",
    long: "Индекс дифференциации",
    color: "orange",
  },
  [PROBLEMS.CORRECT_ANSWERS]: {
    short: "Сложность",
    long: "Слишком мало правильных ответов",
    color: "geekblue",
  },
};

interface ProblemTagProps {
  type: PROBLEMS;
}

export const ProblemTag = ({ type }: ProblemTagProps): JSX.Element => {
  const currentProblem = Problems[type];
  return (
    <>
      <Tooltip title={currentProblem.long}>
        <Tag color={currentProblem.color}>{currentProblem.short}</Tag>
      </Tooltip>
    </>
  );
};
