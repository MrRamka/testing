import React from "react";
import { StatisticsCard } from "./StatisticsCard";
import { Column } from "@ant-design/charts";
import { TestInfoBlock } from "./TestInfoBlock";
import { useQuery } from "react-query";
import { getStatisticsTestAnswersAPI } from "../../constants";
import { useRecoilValue } from "recoil";
import { currentTestValue } from "../../recoil/selectors";
import { TestAnswersStatistics } from "./types";
import { EmptyChartWrapper } from "./EmptyChartWrapper";

export const TestStatisticsCard = (): JSX.Element => {
  const currentTest = useRecoilValue(currentTestValue);

  const { data, isLoading } = useQuery<TestAnswersStatistics>(
    getStatisticsTestAnswersAPI(currentTest.hash),
    {
      enabled: currentTest.hash.length > 0,
    }
  );

  return (
    <StatisticsCard cardTitle={"Статистика теста"}>
      <div className='flex space-x-4'>
        <TestInfoBlock
          primaryText={"Респондентов"}
          secondaryText={`${data?.sessions}`}
        />
        <TestInfoBlock
          primaryText={"Макс баллов"}
          secondaryText={`${data?.test_points}`}
        />
      </div>
      <p className='text-2xl font-semibold my-4 text-center'>
        Распределение баллов
      </p>
      <EmptyChartWrapper
        isEmpty={data?.sessions_distribution?.length === 0}
        isLoading={isLoading}
        description={"Нет ответов на тест"}
      >
        <Column
          xField='score'
          yField='count'
          data={data?.sessions_distribution ?? []}
          height={250}
          yAxis={{
            label: {
              autoEllipsis: true,
              autoHide: true,
              style: {
                fontSize: 14,
              },
            },
            title: {
              text: "Респондентов",
            },
          }}
          xAxis={{
            label: {
              autoEllipsis: true,
              autoHide: true,
              style: {
                fontSize: 14,
              },
            },
            title: {
              text: "Получено баллов",
            },
          }}
          meta={{
            count: {
              alias: "Количество респондентов",
            },
            score: {
              alias: "Получено баллов",
            },
          }}
          renderer='svg'
        />
      </EmptyChartWrapper>
    </StatisticsCard>
  );
};
