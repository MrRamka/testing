export const mockTestAnswersData = [
  {
    value: 0,
    title: "1",
  },
  {
    value: 1,
    title: "2",
  },
  {
    value: 3,
    title: "3",
  },
  {
    value: 5,
    title: "4",
  },
  {
    value: 6,
    title: "5",
  },
  {
    value: 5,
    title: "6",
  },
  {
    value: 2,
    title: "7",
  },
  {
    value: 1,
    title: "8",
  },
];
