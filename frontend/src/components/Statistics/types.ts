export interface TestAnswersDistribution {
  score: number;
  count: number;
}

export interface TestAnswersStatistics {
  test_points: number;
  sessions: number;
  sessions_distribution: TestAnswersDistribution[];
}

export interface TestQuestionPaginationConfig {
  question_count: number;
  question_order_ids: number[];
}
