export const getConstrictorTestQuestionsAPI = (hash: string): string => {
  return `/core/test-questions/${hash}/`;
};

export const getConstrictorTestAPI = (hash: string): string => {
  return `/core/test/${hash}/`;
};

export const getConstrictorTestRunAPI = (hash: string): string => {
  return `/core/test-run/${hash}/`;
};

export const getCoreQuestionsAPI = (hash: string, id: number): string => {
  return `/core/questions/${hash}/${id}/`;
};

export const getCoreQuestionAnswersAPI = (hash: string, id: number): string => {
  return `/core/question-answer/${hash}/${id}/`;
};

export const getCoreCreateQuestionAnswersAPI = (hash: string): string => {
  return `/core/question-answer/${hash}/new/`;
};

export const getCreateTestAPI = (): string => {
  return "/core/test-new/";
};

export const getCreateAnswerAPI = (): string => {
  return "/core/answer-new/";
};

export const getCreateQuestionAPI = (): string => {
  return "/core/question-new/";
};

export const getTestQuestionAnswersAPI = (hash: string, id: number): string => {
  return `/core/test-question-answers/${hash}/${id}/`;
};

export const getCreateSessionAPI = (): string => {
  return `/core/session/start/`;
};

export const getCoreUserQuestionsAPI = (hash: string): string => {
  return `core/test-users-questions/${hash}/`;
};

export const getCoreUserAnswerAPI = (
  hash: string,
  questionId: number
): string => {
  return `/core/user-question-answer/${hash}/${questionId}/`;
};

export const getEndSessionAPI = (): string => {
  return `/core/session/`;
};

export const checkUserAuthAPI = (): string => {
  return `/core/check-user-auth/`;
};

export const getTestQuestionPaginationConfigAPI = (hash: string): string => {
  return `/core/test-question-pagination/${hash}/`;
};

export const getStatisticsQuestionAnswersAPI = (
  hash: string,
  id: number
): string => {
  return `/statistics/question-answers/${hash}/${id}/`;
};

export const getStatisticsTestAnswersAPI = (hash: string): string => {
  return `/statistics/test-points/${hash}/`;
};
