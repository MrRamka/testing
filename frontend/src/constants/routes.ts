export enum Routes {
  LOGIN = "/login/",
  LOGOUT = "/logout/",
  REGISTRATION = "/registration/",
  MAIN = "/main/",
  CONSTRUCTOR = "/constructor/:id",
  CONSTRUCTOR_NEW = "/constructor/new/",
  CONSTRUCTOR_PREFIX = "/constructor/",
  START_TEST = "/run-test/:id",
  START_TEST_PREFIX = "/run-test/",
}
