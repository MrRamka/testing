import React from "react";
import { Input } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { StyledContent } from "./styles";
import { TestsList } from "../../components/TestsList";
import { CardTestItem } from "./types";
import { useQuery } from "react-query";
import { PageHeader } from "../../components/PageHeader";

export const Main = (): JSX.Element => {
  const { data, isLoading } = useQuery<CardTestItem[]>("core/all-users-tests/");

  const routes = [
    {
      path: "/",
      breadcrumbName: "Мои тесты",
    },
  ];

  return (
    <StyledContent>
      <PageHeader routes={routes} title='Ваши тесты' isLoading={isLoading}>
        <Input
          size='middle'
          placeholder='Поиск теста'
          className='w-1/2 rounded-lg '
          style={{ marginLeft: 24, marginTop: 12, marginBottom: 12 }}
          prefix={<SearchOutlined />}
        />
      </PageHeader>

      {/*<MainPageControls onClick={onClick} />*/}

      <TestsList data={data ?? []} isLoading={isLoading} />
    </StyledContent>
  );
};
