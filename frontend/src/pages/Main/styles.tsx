import styled from "styled-components";
import { Layout } from "antd";

const { Content } = Layout;

export const StyledContent = styled(Content)`
  min-height: 280px;
  background-color: #f0f5ff;
`;
