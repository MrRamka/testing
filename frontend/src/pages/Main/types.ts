export interface CardTestItem {
  id: number;
  name: string;
  description?: string;
  questionCount?: number;
  created_at?: string;
  updated_at?: string;
  is_required?: boolean;
  random_order?: boolean;
  isActive?: boolean;
  public?: boolean;
  question_order?: number[];
  hash: string;
  slug?: string;
}
