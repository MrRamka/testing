import { atom } from "recoil";
import { CardTestItem } from "../pages";
import { defaultTest } from "./default";

export const currentTest = atom<CardTestItem>({
  key: "currentTest",
  default: defaultTest,
});
